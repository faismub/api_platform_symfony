Vorraussetzungen um die API auf deinem System zum zu laufen zu bringen:

- PHP Version >8.0
- Composer Version >2.2

Schritte um die API zu installieren: 

Führe folgende Befehle aus: 
 - composer install
 - php bin/console doctrine:database:create
 - php bin/console doctrine:schema:create

Sollten die letzten beiden Befehle zur Erstellung der Datenbank nicht funktionieren, 
führe folgendes aus: 
- php bin/console console doctrine:schema:update --dump-sql
- php bin/console doctrine:schema:update --force 

Es wird eine SQLite Datenbank verwendet. Überprüfe bitte die Zugriffsberechtungen im Ordner /API_Projekt/var/.
Möchtest du eine MySQL Datenbank verwenden, kannst du dies in der .env Datei ändern. 
Mehr hierzu: https://symfony.com/doc/current/doctrine.html#configuring-the-database

Starte nun den Server mit dem Befehl aus dem Projektpfad: 

 - php -S 127.0.0.1:8080 -t public


Wenn du alles eingerichtet und den Server gestartet hast, besuche bitte die Seite 127.0.0.1:8080/testuser/create um einen Benutzer zu erstellen. 
Du findest den Code dafür unter /src/Controller/ApiUserController.php.

Du findest die API-Beschreibung unter der URL  127.0.0.1:8080/api/docs, wo du dich mit admin:admin einloggen kannst.
