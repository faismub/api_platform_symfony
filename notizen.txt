install cmd-befehle:
composer create-project symfony/skeleton security_of_webapps
composer req api
composer req maker

in .env:
DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.sqlite"


php bin/console doctrine:database:create
php bin/console doctrine:schema:create

Falls oben nicht funktioniert dann:
php bin/console console doctrine:schema:update --dump-sql
php bin/console doctrine:schema:update --force


php bin/console make:entity Customer
php bin/console doctrine:schema:create

composer req jwt-auth
php bin/console lexik:jwt:generate-keypair
php bin/console make:user //ApiUser nötig für auth später

composer require --dev symfony/test-pack symfony/http-client justinrainbow/json-schema
composer require --dev hautelook/alice-bundle

security mechanismen befehle schritt:

bei /config/packages/security.yaml folgendes hinzugefügt:
        main:
            lazy: true
            provider: app_user_provider
            json_login:
                check_path: /authentication_token
                username_path: email
                password_path: password
                success_handler: lexik_jwt_authentication.handler.authentication_success
                failure_handler: lexik_jwt_authentication.handler.authentication_failure
            jwt: ~
        api:
            pattern: ^/api/
            stateless: true
            provider: app_user_provider
            jwt: ~

    access_control:
        # - { path: ^/admin, roles: ROLE_ADMIN }
        # - { path: ^/profile, roles: ROLE_USER }
        - { path: ^/docs, roles: PUBLIC_ACCESS } # Allows accessing the Swagger UI
        - { path: ^/authentication_token, roles: PUBLIC_ACCESS }
        - { path: ^/api, roles: IS_AUTHENTICATED_FULLY }

bei config/packages/api_platform.yaml folgendes hinzugefügt:
    swagger:
        versions: [3]
        api_keys:
            apiKey:
                name: Authorization
                type: header

bei config/routes.yaml folgendes hinzugefügt um swagger unter /docs sichtbar zu machen:
swagger_ui:
    path: /docs
    controller: api_platform.swagger.action.ui

und...

authentication_token:
    path: /authentication_token
    methods: ['POST']


damit der authentication_token pfad in swagger angezeigt wird:
in config/services.yaml:
    App\OpenApi\JwtDecorator:
        decorates: 'api_platform.openapi.factory'
        arguments: ['@.inner']

danach src\OpenApi\JwtDecorator erstellen und code aus api platform hinzufügen.