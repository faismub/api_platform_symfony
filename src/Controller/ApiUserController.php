<?php

namespace App\Controller;

use App\Entity\ApiUser;
use Doctrine\Persistence\ManagerRegistry;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class ApiUserController extends AbstractController
{
    #[Route('/testuser/create', name: 'app_api_user_create')]
    public function index(UserPasswordHasherInterface $passwordHasher, ManagerRegistry $doctrine): Response
    {
        $entityManager = $doctrine->getManager();

        $user = new ApiUser();
        $user->setUsername("admin");

        $hashedPassword = $passwordHasher->hashPassword(
            $user,
            'admin'
        );

        $user->setPassword($hashedPassword);
        $user->setRoles([
            'ROLES_ADMIN',
        ]);

        $entityManager->persist($user);
        $entityManager->flush();

        return $this->json([
            'USER' => 'admin',
            'PASSWORD' => 'admin',
        ]);
    }

    #[Route('/user/', name: 'app_api_user')]
    public function user(ManagerRegistry $doctrine): Response
    {
        $userRepository = $doctrine->getRepository(ApiUser::class);
        $user = $userRepository->findAll();

        return $this->json([
            $user
        ]);
    }
}
